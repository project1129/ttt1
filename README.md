### Technical Test Task 1

1) **Add these environment variables to Gitlab CI/CD**  
AWS_ACCESS_KEY_ID  
AWS_SECRET_ACCESS_KEY  
AWS_DEFAULT_REGION

2) **Clone the repository and run Terraform playbook from terraform\init directory**  
This will create:
 - S3 bucket for Terraform backend
 - IAM users and groups
 - SSH keys for administrator and developers (will be stored in AWS SSM Parameter store)
 - RDS password (will be stored in AWS SSM Parameter store)

        cd terraform\init
        terraform init && terraform apply

3) **Run Gitlab CI/CD pipeline**  
In the artifacts will be bastion host IP and AWS ELB fqdn with phpMyAmdmin running behind it.

For example: http://elb-phpmyadmin-262594477.eu-central-1.elb.amazonaws.com/phpmyadmin/  
username: admin  
password: can be found in AWS SSM Parameter store under name /<environment>/mysql  

4) To connect to phpMyAdmin hosts through the bastion host use these command:

        eval $(ssh-agent)
        ssh-add developer1.pem

        ssh -J developer1@<bastion_ip>:27542 developer1@<host_ip> -i developer1.pem  
where developer1.pem is developer private key that can be found in AWS SSM Parameter store
