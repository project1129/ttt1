#####################
### RDS resources ###
#####################

resource "aws_security_group" "mysql_db" {
  name   = "SG-DB"
  vpc_id = aws_vpc.main.id

  ingress {
    from_port       = var.rds_config.port
    to_port         = var.rds_config.port
    protocol        = "tcp"
    security_groups = [aws_security_group.phpMyAdmin_web_access.id]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.all_networks]
  }

  tags       = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]SG-DB Access" })
  depends_on = [aws_security_group.phpMyAdmin_web_access]
}

data "aws_ssm_parameter" "my_rds_password" {
  name = var.rds_config.master_password_name
}

resource "aws_db_subnet_group" "default" {
  name       = "db-subnet-group"
  subnet_ids = aws_subnet.db_subnets.*.id

  tags = merge(var.common_tags, { Name = "${var.common_tags["Environment"]}-DB subnet group" })
}

resource "aws_db_instance" "default" {
  identifier             = var.rds_config.identifier
  allocated_storage      = var.rds_config.allocated_storage
  engine                 = var.rds_config.engine
  engine_version         = var.rds_config.engine_version
  instance_class         = var.rds_config.instance_class
  name                   = var.rds_config.name
  username               = var.rds_config.username
  password               = data.aws_ssm_parameter.my_rds_password.value
  parameter_group_name   = var.rds_config.parameter_group_name
  db_subnet_group_name   = aws_db_subnet_group.default.name
  skip_final_snapshot    = var.rds_config.skip_final_snapshot
  apply_immediately      = var.rds_config.apply_immediately
  vpc_security_group_ids = [aws_security_group.mysql_db.id]

  tags       = merge(var.common_tags, { Name = "${var.common_tags["Environment"]} - MySQL DB" })
  depends_on = [aws_security_group.mysql_db]
}
