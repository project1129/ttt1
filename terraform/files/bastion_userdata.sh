#!/bin/bash
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
sudo apt-get update && sudo apt-get upgrade -y
sudo echo "Port 27542" >> /etc/ssh/sshd_config
sudo systemctl restart sshd
[ -e /var/run/reboot-required ] && sudo reboot
