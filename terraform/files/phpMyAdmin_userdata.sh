#!/bin/bash
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
sudo apt-get update && sudo apt-get upgrade -y
sudo apt-get install apache2 -y
sudo echo "<p>ASG please do not kill me!</p>" >> /var/www/html/index.html
sudo systemctl enable apache2
sudo systemctl start apache2
[ -e /var/run/reboot-required ] && sudo reboot
