variable "region" {
  type    = string
  default = "eu-central-1"
}

variable "common_tags" {
  description = "Tags for all resources"
  type        = map(string)
  default = {
    Owner       = "Admin"
    Project     = "Project name here"
    Environment = "Dev"
  }
}

variable "access_key" {
  type        = string
  description = "AWS access key"
  sensitive   = true
}

variable "secret_key" {
  type        = string
  description = "AWS secret key"
  sensitive   = true
}

variable "state_bucket_name" {
  type        = string
  description = "AWS S3 bucket name for storing terraform remote state"
  default     = "project-ttt1-terraform-state"
}

variable "admin_ssh_key" {
  type = object({
    key_pair_name        = string
    private_key_ssm_name = string
    public_key_ssm_name  = string
  })
  default = {
    key_pair_name        = "admin-ssh-keypair"
    private_key_ssm_name = "/admin_ssh_key/private_key"
    public_key_ssm_name  = "/admin_ssh_key/public_key"
  }
}

variable "dev1_ssh_key" {
  type = object({
    key_pair_name        = string
    private_key_ssm_name = string
    public_key_ssm_name  = string
  })
  default = {
    key_pair_name        = "dev1-ssh-keypair"
    private_key_ssm_name = "/dev1_ssh_key/private_key"
    public_key_ssm_name  = "/dev1_ssh_key/public_key"
  }
}

variable "dev2_ssh_key" {
  type = object({
    key_pair_name        = string
    private_key_ssm_name = string
    public_key_ssm_name  = string
  })
  default = {
    key_pair_name        = "dev2-ssh-keypair"
    private_key_ssm_name = "/dev2_ssh_key/private_key"
    public_key_ssm_name  = "/dev2_ssh_key/public_key"
  }
}

variable "rds_password" {
  type = object({
    name             = string
    length           = number
    special          = bool
    override_special = string
  })
  default = {
    name             = "/dev/mysql"
    length           = 12
    special          = true
    override_special = "!&*%#"
  }
}
