## Master password for RDS database
resource "random_string" "rds_password" {
  length           = var.rds_password.length
  special          = var.rds_password.special
  override_special = var.rds_password.override_special
}

resource "aws_ssm_parameter" "rds_password" {
  name        = var.rds_password.name
  description = "Master password for RDS database"
  type        = "SecureString"
  value       = random_string.rds_password.result
}
