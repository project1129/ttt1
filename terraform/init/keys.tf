## Administrator key pair
resource "tls_private_key" "admin_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_ssm_parameter" "admin_private_key" {
  name        = var.admin_ssh_key.private_key_ssm_name
  description = "SSH private key"
  type        = "SecureString"
  value       = tls_private_key.admin_key.private_key_pem
}

resource "aws_ssm_parameter" "admin_public_key" {
  name        = var.admin_ssh_key.public_key_ssm_name
  description = "SSH public key"
  type        = "SecureString"
  value       = tls_private_key.admin_key.public_key_openssh
}

resource "aws_key_pair" "admin_ssh_key" {
  key_name   = var.admin_ssh_key.key_pair_name
  public_key = tls_private_key.admin_key.public_key_openssh
  tags       = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]Administrator keypair for phpMyAdmin instances" })
}

## Developers key pairs
#Developer 1
resource "tls_private_key" "dev1_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_ssm_parameter" "dev1_private_key" {
  name        = var.dev1_ssh_key.private_key_ssm_name
  description = "SSH private key"
  type        = "SecureString"
  value       = tls_private_key.dev1_key.private_key_pem
}

resource "aws_ssm_parameter" "dev1_public_key" {
  name        = var.dev1_ssh_key.public_key_ssm_name
  description = "SSH public key"
  type        = "SecureString"
  value       = tls_private_key.dev1_key.public_key_openssh
}

resource "aws_key_pair" "dev1_ssh_key" {
  key_name   = var.dev1_ssh_key.key_pair_name
  public_key = tls_private_key.dev1_key.public_key_openssh
  tags       = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]Developer 1 keypair for phpMyAdmin instances" })
}

#Developer 2
resource "tls_private_key" "dev2_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_ssm_parameter" "dev2_private_key" {
  name        = var.dev2_ssh_key.private_key_ssm_name
  description = "SSH private key"
  type        = "SecureString"
  value       = tls_private_key.dev2_key.private_key_pem
}

resource "aws_ssm_parameter" "dev2_public_key" {
  name        = var.dev2_ssh_key.public_key_ssm_name
  description = "SSH public key"
  type        = "SecureString"
  value       = tls_private_key.dev2_key.public_key_openssh
}

resource "aws_key_pair" "dev2_ssh_key" {
  key_name   = var.dev2_ssh_key.key_pair_name
  public_key = tls_private_key.dev2_key.public_key_openssh
  tags       = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]Develoer 2 keypair for phpMyAdmin instances" })
}
