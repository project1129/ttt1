#############################
### Bastion host instance ###
#############################
resource "aws_security_group" "bastion_access" {
  name   = "SG-Bastion"
  vpc_id = aws_vpc.main.id

  dynamic "ingress" {
    for_each = var.bastion_instance_config.allow_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = var.bastion_instance_config.allow_ips
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.all_networks]
  }

  tags = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]SG-Bastion access via SSH" })

}

resource "aws_instance" "bastion" {
  ami                    = data.aws_ami.latest_ubuntu.id
  key_name               = var.bastion_instance_config.key_pair_name
  instance_type          = var.bastion_instance_config.instance_type
  vpc_security_group_ids = [aws_security_group.bastion_access.id]
  subnet_id              = aws_subnet.public_subnets[0].id
  #  iam_instance_profile   = aws_iam_instance_profile.bastion_profile.name
  user_data = file(var.bastion_instance_config.user_data_file)

  tags = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]Bastion host", Type = "Bastion" })
}
