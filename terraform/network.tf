## Create VPC and all networks needed
data "aws_availability_zones" "available" {}

resource "aws_vpc" "main" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags                 = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]VPC" })
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
  tags   = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]IGW" })
}

resource "aws_subnet" "public_subnets" {
  count                   = length(var.public_subnet_cidrs)
  vpc_id                  = aws_vpc.main.id
  cidr_block              = element(var.public_subnet_cidrs, count.index)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true
  tags                    = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]Public subnet-${count.index + 1}" })
}

resource "aws_subnet" "private_subnets" {
  count                   = length(var.private_subnet_cidrs)
  vpc_id                  = aws_vpc.main.id
  cidr_block              = element(var.private_subnet_cidrs, count.index)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = false
  tags                    = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]Private subnet-${count.index + 1}" })
}

resource "aws_subnet" "db_subnets" {
  count                   = length(var.db_subnet_cidrs)
  vpc_id                  = aws_vpc.main.id
  cidr_block              = element(var.db_subnet_cidrs, count.index)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = false
  tags                    = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]DB subnet-${count.index + 1}" })
}


resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }
  tags = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]Routes for public subnets via IGW" })
}

resource "aws_route_table_association" "public_routes" {
  count          = length(aws_subnet.public_subnets[*].id)
  route_table_id = aws_route_table.public_route_table.id
  subnet_id      = element(aws_subnet.public_subnets[*].id, count.index)
}

resource "aws_eip" "nat_eip" {
  count = length(var.private_subnet_cidrs)
  vpc   = true
  tags  = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]NAT GW IP ${count.index + 1}" })
}

resource "aws_nat_gateway" "nat" {
  count         = length(var.private_subnet_cidrs)
  allocation_id = aws_eip.nat_eip[count.index].id
  subnet_id     = element(aws_subnet.public_subnets[*].id, count.index)
  tags          = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]NAT GW ${count.index + 1}" })

  depends_on = [aws_internet_gateway.main]
}

resource "aws_route_table" "nat_route_table" {
  count  = length(var.private_subnet_cidrs)
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat[count.index].id
  }
  tags = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]Routes for private subnets via NAT GW ${count.index + 1}" })
}

resource "aws_route_table_association" "private_routes" {
  count          = length(aws_subnet.private_subnets[*].id)
  route_table_id = aws_route_table.nat_route_table[count.index].id
  subnet_id      = element(aws_subnet.private_subnets[*].id, count.index)
}

resource "aws_route_table_association" "db_routes" {
  count          = length(aws_subnet.db_subnets[*].id)
  route_table_id = aws_route_table.nat_route_table[count.index].id
  subnet_id      = element(aws_subnet.db_subnets[*].id, count.index)
}
