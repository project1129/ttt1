###################################
### Instances and ELB resources ###
###################################
resource "aws_security_group" "phpMyAdmin_web_access" {
  name   = "SG-phpMyAdmin"
  vpc_id = aws_vpc.main.id

  dynamic "ingress" {
    for_each = var.phpMyAdmin_instance_config.allow_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = [var.all_networks]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.all_networks]
  }

  tags = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]SG-phpMyAdmin Web Access" })
}

resource "aws_security_group" "allow_bastion" {
  name   = "SG-Allow-bastion"
  vpc_id = aws_vpc.main.id

  ingress {
    from_port       = var.ssh_port
    to_port         = var.ssh_port
    protocol        = "tcp"
    security_groups = [aws_security_group.bastion_access.id]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.all_networks]
  }

  tags = merge(var.common_tags, { Name = "[${var.common_tags["Environment"]}]SG-Allow Bastion host access" })
}


resource "aws_launch_configuration" "phpMyAdmin_instances" {
  name_prefix       = var.launch_configuration_config.name_prefix
  image_id          = data.aws_ami.latest_ubuntu.id
  instance_type     = var.phpMyAdmin_instance_config.instance_type
  key_name          = var.phpMyAdmin_instance_config.key_pair_name
  security_groups   = [aws_security_group.phpMyAdmin_web_access.id, aws_security_group.allow_bastion.id]
  user_data         = file(var.phpMyAdmin_instance_config.user_data_file)
  enable_monitoring = var.launch_configuration_config.detailed_monitoring

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "phpMyAdmin_instances" {
  name                 = "ASG-${aws_launch_configuration.phpMyAdmin_instances.name}"
  launch_configuration = aws_launch_configuration.phpMyAdmin_instances.name
  min_size             = var.autoscaling_group_config.min_size
  max_size             = var.autoscaling_group_config.max_size
  min_elb_capacity     = var.autoscaling_group_config.min_elb_capacity
  health_check_type    = var.autoscaling_group_config.health_check_type
  vpc_zone_identifier  = aws_subnet.private_subnets.*.id
  load_balancers       = [aws_elb.phpMyAdmin_elb.name]

  tag {
    key                 = "Name"
    value               = "[${var.common_tags["Environment"]}]phpMyAdmin instance"
    propagate_at_launch = true
  }

  tag {
    key                 = "Type"
    value               = "phpMyAdmin"
    propagate_at_launch = true
  }

  dynamic "tag" {
    for_each = var.common_tags
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_elb" "phpMyAdmin_elb" {
  name    = "ELB-phpMyAdmin"
  subnets = aws_subnet.public_subnets.*.id
  #  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  security_groups = [aws_security_group.phpMyAdmin_web_access.id]
  listener {
    lb_port           = var.elb_config.lb_port
    lb_protocol       = var.elb_config.lb_protocol
    instance_port     = var.elb_config.instance_port
    instance_protocol = var.elb_config.instance_protocol
  }
  health_check {
    healthy_threshold   = var.elb_config.healthy_threshold
    unhealthy_threshold = var.elb_config.unhealthy_threshold
    timeout             = var.elb_config.timeout
    target              = var.elb_config.target
    interval            = var.elb_config.interval
  }

  tags = var.common_tags
}
