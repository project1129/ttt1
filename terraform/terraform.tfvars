## Common variables
region = "eu-central-1"
common_tags = {
  Owner       = "Daniel"
  Project     = "Technical Test Task 1"
  Environment = "Dev"
}
ubuntu_ami_owners  = ["099720109477"]
ubuntu_ami_filters = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]

## Network
vpc_cidr             = "10.0.0.0/16"
public_subnet_cidrs  = ["10.0.1.0/24", "10.0.2.0/24"]
private_subnet_cidrs = ["10.0.10.0/24", "10.0.11.0/24"]
db_subnet_cidrs      = ["10.0.20.0/24", "10.0.21.0/24"]

ssh_port = 22

## Bastion instance variables
bastion_instance_config = {
  instance_type  = "t3.micro"
  allow_ports    = ["27542"]
  allow_ips      = ["0.0.0.0/0"]
  user_data_file = "files/bastion_userdata.sh"
  key_pair_name  = "admin-ssh-keypair"
}

## phpMyAdmin instance variables
phpMyAdmin_instance_config = {
  instance_type  = "t3.micro"
  allow_ports    = ["80"]
  user_data_file = "files/phpMyAdmin_userdata.sh"
  key_pair_name  = "admin-ssh-keypair"
}

launch_configuration_config = {
  name_prefix         = "LC-phpMyAdmin-web_instances"
  detailed_monitoring = "false"
}

autoscaling_group_config = {
  name_prefix       = "LC-phpMyAdmin-web_instances"
  min_size          = 1
  max_size          = 1
  min_elb_capacity  = 1
  health_check_type = "ELB"
}

## ELB variables
elb_config = {
  lb_port             = 80
  lb_protocol         = "http"
  instance_port       = 80
  instance_protocol   = "http"
  healthy_threshold   = 2
  unhealthy_threshold = 2
  timeout             = 3
  target              = "HTTP:80/"
  interval            = 10
}

## RDS variables
rds_config = {
  port                 = 3306
  master_password_name = "/dev/mysql"
  identifier           = "rds-phpmyadmin"
  allocated_storage    = 10
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t3.micro"
  name                 = "mydb"
  username             = "admin"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
  apply_immediately    = true
}
