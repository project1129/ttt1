provider "aws" {
  #access_key = var.access_key
  #secret_key = var.secret_key
  region = var.region
}

terraform {
  backend "s3" {
    bucket = "project-ttt1-terraform-state"
    key    = "dev/terraform.tfstate"
    region = "eu-central-1"
  }
}

data "aws_ami" "latest_ubuntu" {
  owners      = var.ubuntu_ami_owners
  most_recent = true
  filter {
    name   = "name"
    values = var.ubuntu_ami_filters
  }
}
