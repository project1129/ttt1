## Common variables
# variable "access_key" {
#   type        = string
#   description = "AWS access key"
#   sensitive   = true
# }
#
# variable "secret_key" {
#   type        = string
#   description = "AWS secret key"
#   sensitive   = true
# }

variable "region" {
  type    = string
  default = "eu-central-1"
}

variable "common_tags" {
  description = "Tags for all resources"
  type        = map(string)
  default = {
    Owner       = "Admin"
    Project     = "Project name here"
    Environment = "Dev"
  }
}

variable "ubuntu_ami_owners" {
  default = ["099720109477"]
}

variable "ubuntu_ami_filters" {
  default = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
}

## Network
variable "all_networks" {
  description = "List of ports to open"
  type        = string
  default     = "0.0.0.0/0"
}

variable "vpc_cidr" {
  type    = string
  default = "10.0.0.0/16"
}

variable "public_subnet_cidrs" {
  type = list(string)
  default = [
    "10.0.1.0/24",
    "10.0.2.0/24",
  ]
}

variable "private_subnet_cidrs" {
  type = list(string)
  default = [
    "10.0.3.0/24",
    "10.0.4.0/24",
  ]
}

variable "db_subnet_cidrs" {
  type = list(string)
  default = [
    "10.0.5.0/24",
    "10.0.6.0/24",
  ]
}

variable "ssh_port" {
  type    = string
  default = 22
}

## Bastion instance variables
variable "bastion_instance_config" {
  type = object({
    instance_type  = string
    allow_ports    = list(number)
    allow_ips      = list(string)
    user_data_file = string
    key_pair_name  = string
  })
  default = {
    instance_type  = "t3.micro"
    allow_ports    = ["27542"]
    allow_ips      = ["0.0.0.0/0"]
    user_data_file = "bastion_userdata.sh"
    key_pair_name  = "admin-ssh-keypair"
  }
}

## phpMyAdmin instance variables
variable "phpMyAdmin_instance_config" {
  type = object({
    instance_type  = string
    allow_ports    = list(number)
    user_data_file = string
    key_pair_name  = string
  })
  default = {
    instance_type  = "t3.micro"
    allow_ports    = ["80"]
    user_data_file = "phpMyAdmin_userdata.sh"
    key_pair_name  = "admin-ssh-keypair"
  }
}

variable "launch_configuration_config" {
  type = object({
    name_prefix         = string
    detailed_monitoring = bool
  })
  default = {
    name_prefix         = "LC-phpMyAdmin-web_instances"
    detailed_monitoring = "false"
  }
}

variable "autoscaling_group_config" {
  type = object({
    name_prefix       = string
    min_size          = number
    max_size          = number
    min_elb_capacity  = number
    health_check_type = string
  })
  default = {
    name_prefix       = "LC-phpMyAdmin-web_instances"
    min_size          = 1
    max_size          = 1
    min_elb_capacity  = 1
    health_check_type = "ELB"
  }
}

## ELB variables
variable "elb_config" {
  type = object({
    lb_port             = number
    lb_protocol         = string
    instance_port       = number
    instance_protocol   = string
    healthy_threshold   = number
    unhealthy_threshold = number
    timeout             = number
    target              = string
    interval            = number
  })
  default = {
    lb_port             = 80
    lb_protocol         = "http"
    instance_port       = 80
    instance_protocol   = "http"
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 10
  }
}

## RDS variables
variable "rds_config" {
  type = object({
    port                 = number
    master_password_name = string
    identifier           = string
    allocated_storage    = number
    engine               = string
    engine_version       = string
    instance_class       = string
    name                 = string
    username             = string
    parameter_group_name = string
    skip_final_snapshot  = bool
    apply_immediately    = bool
  })
  default = {
    port                 = 3306
    master_password_name = "/dev/mysql"
    identifier           = "rds-phpmyadmin"
    allocated_storage    = 10
    engine               = "mysql"
    engine_version       = "5.7"
    instance_class       = "db.t3.micro"
    name                 = "mydb"
    username             = "admin"
    parameter_group_name = "default.mysql5.7"
    skip_final_snapshot  = true
    apply_immediately    = true
  }
}
