output "bastion_host_ip" {
  value = aws_instance.bastion.public_ip
}
